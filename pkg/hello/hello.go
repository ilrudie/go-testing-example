package hello

import (
	"fmt"
	"strings"
)

// Say is an opinionated hello function
func Say(input string) (string, error) {
	if input == "" {
		return "", fmt.Errorf("hello.Say won't say hello to nothing")
	} else if strings.Contains(strings.ToLower(input), "java") {
		return "humph", fmt.Errorf("hello.Say doesn't respect Java and won't say hello")
	} else {
		return fmt.Sprintf("Hello, %s!", input), nil
	}
}
