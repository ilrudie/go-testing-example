package hello

import (
	"github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/extensions/table"
	"github.com/onsi/gomega"
	"github.com/stretchr/testify/assert"
	"testing"
)

type testcase struct {
	Description string
	Input       string
	Expect      string
	ExpectError bool
}

var cases = []testcase{
	{"Normal", "Go", "Hello, Go!", false},
	{"Java error", "Java", "humph", true},
	{"Empty string error", "", "", true},
	{"Normal", "Testing", "Hello, Testing!", false},
}

func TestSayTestify(t *testing.T) {

	run := func(t *testing.T, tc testcase) {
		t.Run(tc.Description, func(t *testing.T) {
			result, err := Say(tc.Input)
			if tc.ExpectError {
				assert.Error(t, err)
			} else {
				assert.Nil(t, err)
			}
			assert.EqualValues(t, tc.Expect, result)
		})
	}

	for _, tc := range cases {
		run(t, tc)
	}

}

func TestSayGomega(t *testing.T) {

	g := gomega.NewGomegaWithT(t)

	run := func(t *testing.T, tc testcase) {
		t.Run(tc.Description, func(t *testing.T) {
			result, err := Say(tc.Input)
			if tc.ExpectError {
				g.Expect(err).To(gomega.HaveOccurred())
			} else {
				g.Expect(err).ToNot(gomega.HaveOccurred())
			}
			g.Expect(result).Should(gomega.Equal(tc.Expect))
		})
	}

	for _, tc := range cases {
		run(t, tc)
	}

}

func TestSayGinkgo(t *testing.T) {

	gomega.RegisterFailHandlerWithT(t, ginkgo.Fail)
	g := gomega.NewGomegaWithT(t)

	ginkgo.Describe("gitlab.com/ilrudie/go-testing-example/pkg/hello", func() {
		table.DescribeTable("Say Table-driven Test", func(input string, expect string, expectError bool) {
			result, err := Say(input)
			if expectError {
				g.Expect(err).To(gomega.HaveOccurred())
			} else {
				g.Expect(err).ToNot(gomega.HaveOccurred())
			}
			g.Expect(result).Should(gomega.Equal(expect))

		},
			table.Entry("Normal", "Go", "Hello, Go!", false),
			table.Entry("Java error", "Java", "humph", true),
			table.Entry("Empty string error", "", "", true),
			table.Entry("Normal", "Testing", "Hello, Testing!", false))
	})

}
