module gitlab.com/ilrudie/go-testing-example

go 1.14

require (
	github.com/onsi/ginkgo v1.12.1
	github.com/onsi/gomega v1.10.4
	github.com/stretchr/testify v1.6.1
)
