package main

import (
	"fmt"
	"os"

	"gitlab.com/ilrudie/go-testing-example/pkg/hello"
)

func main() {

	msg, err := hello.Say("Go")
	if err != nil {
		fmt.Printf("I got an error: %s\n", err.Error())
		os.Exit(1)
	}

	fmt.Println(msg)

}
